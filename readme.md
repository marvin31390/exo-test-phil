# TDD

- Cloner ce repo
- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)
- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc, 
avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction

principes du tdd :
pour moi le tdd c'est un façon de programmer on commence par établir les tests et ensuite on fait les fonctions qui validerons ces test, une fois les tests validés on rend notre code plus propre ce qui permet d'avoir un code plus facile a maintenir et surtout de ne pas faire sauter toute l'application quand on change un truc dans notre code.
