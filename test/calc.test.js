const expect = require('chai').expect;
const {calc} = require('../src/calc');

describe("calc", () => {
    it("Should return the sum of two numbers", () => {
        expect(calc(0.6, 0.3, "+")).to.equal(0.9);
    });
    it("Should return the soustract of two numbers", () => {
        expect(calc(0.345675433, 0.234543214, "-")).to.equal(0.111132219);
    });
    it("Should return the multiply of two numbers", () => {
        expect(calc(0.3, 0.3, "*")).to.equal(0.09);
    });
    it("Should return the divide of two numbers", () => {
        expect(calc(0, 3, "/")).to.equal(0);
    });
    it("Should return the denominator don't be equal to 0", () => {
        expect(calc(12, 0, "/")).to.equal("the denominator don't be equal to 0")
    })
    
})